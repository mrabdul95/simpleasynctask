package com.abdulrahman.myfirstapplication.simpleasynctask

import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import android.widget.TextView
import java.lang.ref.WeakReference
import kotlin.random.Random


class MainActivity : AppCompatActivity() {
    private var mTextView:TextView?=null
    companion object{
        //key for saving the state of the textView
        private const val TEXT_STATE="currentText"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mTextView=findViewById(R.id.textView1)
        if(savedInstanceState!=null){
            mTextView?.text=savedInstanceState.getString(TEXT_STATE)
        }else{
            mTextView?.text="no saved instance"
        }
    }


    fun startTask(view: View) {
        mTextView?.text="napping"
        mTextView?.let {
            SimpleAsyncTask(it).execute()
        }

    }
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(TEXT_STATE,mTextView?.text.toString())
    }
}
