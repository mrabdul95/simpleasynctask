package com.abdulrahman.myfirstapplication.simpleasynctask

import android.os.AsyncTask
import android.widget.TextView
import java.lang.ref.WeakReference
import kotlin.random.Random

class SimpleAsyncTask(tv: TextView): AsyncTask<Void, Void, String>() {
    // we use a weak reafreence to the text view so the reference is removed once the task is completed alloweieng it to be garbage collected
    //prevents memory leaks
    private val mTextView: WeakReference<TextView> = WeakReference(tv)

    override fun doInBackground(vararg params: Void?): String {
        val r = Random
        val n = r.nextInt(11)
        val s = n * 200
        try {
            Thread.sleep(s.toLong())

        } catch (e: InterruptedException) {
            e.printStackTrace()

        }
        return ("Awake from my slumber at last after resting for $s milliseconds!")
        //the return value is automatically passed to the onPostExecute callback when the doInBackground completes
    }

    override fun onPostExecute(result: String?) {
        super.onPostExecute(result)
        //because the mTextView is weak reffrenece then we must use get() to get the underlying text vuew object
        mTextView.get()?.text=result
    }
}